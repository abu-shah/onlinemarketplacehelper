﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OnlineMarketPlaceHelper.Web.Startup))]
namespace OnlineMarketPlaceHelper.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
